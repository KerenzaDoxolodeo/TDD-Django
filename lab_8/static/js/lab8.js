var uid;
var accessToken;
  // FB initiation function
 $(document).ready(function() {
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '1861058727537750',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11',
      auth: true
    });

    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        uid = response.authResponse.userID;
        accessToken = response.authResponse.accessToken;
        console.log("connected");
        console.log(uid);
        render(true);
      } else if (response.status === 'not_authorized') {
        console.log("not authorized");
        render(false);
      } else {
        console.log("not login");
        render(false);
      }
    });
    console.log("FINISH");
  };
});


  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));


  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    console.log(loginFlag);
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      var fun = getUserData(user => {
        console.log(user);

      var delStrign ="";
        for(var i =0 ; i< 1000000; i ++){
          for(var j =0 ; j< 1000; j ++){
          delStrign = + "hahaha";
          console.log("i");
          }
        }
        console.log("Hahahah");
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('.lab8').html('<div class="card"><div class="card-block">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '</div>');
        $('.lab8').append('<div class="card-block"><input class="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
          '</br><button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
          '<button class="logout" onclick="facebookLogout()">Logout</button></div>'
        );

        $('.lab8').append('<div class="card">');

        $('.lab8').append('<div class="card-block"><h1>' + user.name + '</h1>' +
              '<h2>' + user.about + '</h2>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>'
        );

        console.log("Aufa")

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            if (value.message && value.story) {
              $('.lab8').append(
                '<div class="feed card-block">' +
                  '<h2>' + value.message + '</h1>' +
                  '<h3>' + value.story + '</h2>' +
                '</div>'
              );
            } else if (value.message) {
              $('.lab8').append(
                '<div class="feed card-block">' +
                  '<h2>' + value.message + '</h1>' +
                '</div>'
              );
            } else if (value.story) {
              $('.lab8').append(
                '<div class="feed card-block">' +
                  '<h3>' + value.story + '</h2>' +
                '</div>'
              );
            }
          });
        });
        
        $('.lab8').append('</div>');
      });
    } else {
      console.log("YEAH");
      $('.lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
      console.log($('.lab8'));
    }
  };


  const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    FB.login(function(response) {
      if (response.authResponse) {
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me', function(response) {
          console.log('Good to see you, ' + response.name + '.');
          window.location.reload();
        });
      } else {
       console.log('User cancelled login or did not fully authorize.');
      }
    }, {scope:'public_profile,user_posts,publish_actions'});
  };

  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.logout();
      }
    });
    window.location.reload();
  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?

  const getUserData = (fun) => {
    FB.api("/me?fields=name,about,email,gender,cover,picture",
        function (response) {
          if (response && !response.error) {
            console.log("user data running");
            fun(response);
          }
        }
    );
  };
  
  const getUserFeed = (fun) => {
    FB.api("/me/feed",
      function (response) {
      if (response && !response.error) {
        console.log("user feed running");
        console.log(response);
        fun(response);
      }
    });
  };


  const postFeed = (message) => {
    console.log(message);
    FB.api('/me/feed', 'POST', {message:message});
  };

  const postStatus = () => {
    const message = $('.postInput').val();
    postFeed(message);
    window.location.reload();
  };