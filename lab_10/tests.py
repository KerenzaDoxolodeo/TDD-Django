from django.test import TestCase
from django.test import Client
import environ
from .csui_helper import get_access_token, verify_user, get_client_id, get_data_user
from .omdb_api import search_movie, get_detail_movie

env = environ.Env(DEBUG=(bool, False),)

class Lab10UnitTest(TestCase):
    def test_lab_10_session_url_is_exist(self):
        response = Client().get('/lab-10/')
        self.assertEqual(response.status_code, 200)

    def test_lab_10_session_login_page_is_working(self):
        response = Client().get('/lab-10/')
        html_response = response.content.decode('utf8')
        self.assertIn("Gunakan <b> akun SSO </b> untuk login",html_response)

    def test_dashboard(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
        response = self.client.get('/lab-10/dashboard/',follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('kerenza.doxolodeo',html_response)

    def test_add_watch_later_is_working(self):
        response = Client().get('/lab-10/movie/detail/tt2096673/')
        html_response = response.content.decode('utf8')
        self.assertIn('Inside Out',html_response)
        self.assertIn('Add to Watch Later',html_response)
        response = Client().get('/lab-10/movie/watch_later/add/tt2096673/',follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('Added to Watch Later',html_response)

    def test_csui_helper(self):
        self.assertEqual(get_client_id(),'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')
        verify_user('X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')
        get_access_token('kerenza.doxolodeo','password')
        get_data_user('w','w')
        self.assertTrue(True)

    def test_omdb(self):
        result = search_movie('dunkirk','-')
        self.assertEqual(len(result),10)
        result = get_detail_movie('tt5013056')
        self.assertEqual(result['title'].decode('utf8'),'Dunkirk')