from django.shortcuts import render
from lab_2.views import landing_page_content

from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message
from django.db import transaction
from django.utils.timezone import activate


# Create your views here.
response = {'author': "Kerenza Doxolodeo"} #TODO Implement yourname
about_me = ["I study in Fasilkom UI","I love reading books","I like playing Roblox","=)","I do not know what else to write","Bye!"]
def index(request):
    response['content'] = landing_page_content
    html = 'lab_4/lab_4.html'
    response['about_me'] = about_me
    response['message_form'] = Message_Form
    return render(request, html, response)


def message_post(request):
    form = Message_Form(request.POST or None)
    html ='lab_4/form_result.html'
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        with transaction.atomic():
                message = Message(name=response['name'], email=response['email'],
                          message=response['message'])
                message.save()
        response['bad'] = False
        return render(request, html, response)
    else:
        response['bad'] = True        
        return render(request, html, response)

def message_table(request):
    message = Message.objects.all()
    response['message'] = message
    html = 'lab_4/table.html'
    return render(request, html , response)
