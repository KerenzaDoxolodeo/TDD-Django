from django import forms

class Message_Form(forms.Form):
    my_error_messages = {
        'required': 'Pesan tidak boleh kosong',
        'invalid': 'Kolom ini harus diisi dengan email yang valid',
    }
    attrs = {'class': 'form-control'}
        
    name = forms.CharField(label='Nama', required=False, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(required=False, widget=forms.EmailInput(attrs=attrs),error_messages=my_error_messages)
    message = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True,error_messages=my_error_messages)