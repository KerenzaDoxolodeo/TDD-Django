from django.test import TestCase, RequestFactory
from django.test import Client
from django.urls import resolve
from .views import index, profile, \
    add_session_drones, del_session_drones, clear_session_drones, \
    add_session_soundcards, del_session_soundcards, clear_session_soundcards, \
    add_session_opticals, del_session_opticals, clear_session_opticals, \
    cookie_login, cookie_auth_login, cookie_profile, cookie_clear, \
    is_login, my_cookie_auth
from .api_enterkomputer import get_drones, get_soundcards, get_opticals
from .csui_helper import get_access_token, verify_user, get_client_id, get_data_user
import environ

env = environ.Env(DEBUG=(bool, False),)

class Lab9UnitTest(TestCase):
    def test_lab_9_session_url_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code, 200)

    def test_lab9_session_using_index_func(self):
        found = resolve('/lab-9/')
        self.assertEqual(found.func, index)

    def test_lab_9_cookie_url_is_exist(self):
        response = Client().get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 200)

    def test_lab9_cookie_using_func(self):
        found = resolve('/lab-9/cookie/login/')
        self.assertEqual(found.func, cookie_login)

    def test_lab9_cookie_checks_the_correct_password(self):
        self.assertEqual(my_cookie_auth('utest','ptest'),True)
        self.assertEqual(my_cookie_auth('wow','mom'),False)

    def test_api_enterkomputer(self):
        m = get_drones()
        m = get_soundcards()
        m = get_opticals()
        self.assertTrue(True)

    def test_lab_9_session_login_page_is_working(self):
        response = Client().get('/lab-9/')
        html_response = response.content.decode('utf8')
        self.assertIn("Gunakan <b> akun SSO </b> untuk login",html_response)

    def test_profile(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        response = self.client.get('/lab-9/profile/',follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('kerenza.doxolodeo',html_response)


    def test_lab_9_cookie_login_page_is_working(self):
        response = Client().get('/lab-9/cookie/login/')
        html_response = response.content.decode('utf8')
        self.assertIn("Jangan menggunakan <b> akun SSO asli </b>",html_response)

    def test_lab_9_cookie_bad_login_is_working(self):
        response = Client().post('/lab-9/cookie/auth_login/',{'username':'u','password':'p'}, follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn("Jangan menggunakan <b> akun SSO asli </b>",html_response)

    def test_lab_9_cookie_good_login_is_working(self):
        response = Client().post('/lab-9/cookie/auth_login/',{'username':'utest','password':'ptest'}, follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn("Reset Cookies (Logout)",html_response)

    def test_lab_9_clear_cookie_is_working(self):
        response = Client().get('/lab-9/cookie/clear/')
        self.assertEqual(response.status_code, 302)

    def test_csui_helper(self):
        self.assertEqual(get_client_id(),'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')
        verify_user('X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')
        get_access_token('kerenza.doxolodeo','password')
        get_data_user('w','w')
        self.assertTrue(True)