from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.core import serializers
from django.http import JsonResponse
from .api_csui_helper.csui_helper import CSUIhelper

from .views import index, add_friend, validate_npm, delete_friend, friend_list, friend_list_json
from .models import Friend, Mahasiswa


class Lab7UnitTest(TestCase):

    def test_model_can_create_new_Mahasiswa(self):
        Mahasiswa.objects.create(mahasiswa_name = "Deo", npm = "1606881670")
        Mahasiswa.objects.create(mahasiswa_name = "Setan", npm = "666666")

        counting_all_available_mahasiswa = Mahasiswa.objects.all().count()
        self.assertEqual(counting_all_available_mahasiswa, 2)
    '''
    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_lab7_can_pagination(self):
        response= Client().get('/lab-7/')
        html_response = response.content.decode('utf8')
        self.assertIn("HUGO REYNALDO (0606101452)",html_response)
    '''
    def test_model_can_create_new_Friend(self):
        Friend.objects.create(friend_name = "Deo", npm = "1606881670")
        Friend.objects.create(friend_name = "Setan", npm = "666666")

        counting_all_available_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_available_friend, 2)

    def test_lab_7_friend_list_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)

    def test_lab_7_friend_list_json_is_exist(self):
        response = Client().get('/lab-7/get-friend-list-json/')
        self.assertEqual(response.status_code, 200)
    
    def test_csui_helper(self):
        csui_helper = CSUIhelper()
        csui_helper.instance.get_mahasiswa_list()
        csui_helper.instance.get_auth_param_dict()
        csui_helper.instance.get_client_id()
        csui_helper.instance.get_access_token()
        csui_helper.instance.get_auth_param_dict()
        self.assertTrue(True)
