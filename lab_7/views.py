from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend, Mahasiswa
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()

    for mahasiswaEntry in mahasiswa_list:
        Mahasiswa.objects.get_or_create(mahasiswa_name=mahasiswaEntry['nama'],npm=mahasiswaEntry['npm'])

    friend_list = Friend.objects.all()
    mahasiswa_list = Mahasiswa.objects.all()

    paginator = Paginator(mahasiswa_list, 25)

    page = request.GET.get('page')
    try:
        collected = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        collected = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        collected = paginator.page(paginator.num_pages)

    response = {"mahasiswa_list": collected, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar-teman.html'
    return render(request, html, response)

def friend_list_json(request):
    SomeModel_json = serializers.serialize("json", Friend.objects.all())
    data = {"SomeModel_json": SomeModel_json}
    return JsonResponse(data)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        data = model_to_dict(friend)
        return HttpResponse(data)

@csrf_exempt
def delete_friend(request):
    if request.method == 'POST':
        npm = request.POST.get('npm',None)
        print(npm)
        if npm != None:
            Friend.objects.filter(npm=npm).delete()
            data = {'ok': True}
            return JsonResponse(data)

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    entry = Friend.objects.filter(npm = npm)
    data = {
        'is_taken': len(entry) >= 1
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data
