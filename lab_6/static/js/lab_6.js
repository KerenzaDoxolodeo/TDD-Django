  var print = document.getElementById('print');
  var erase = false;

  var themes = `[
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]`

localStorage.setItem("themes",themes)

var go = function(x) {
    if (x === 'ac') {
        if (erase){
          print.value = "";
          erase = false;
        }
    } else if (x === 'eval') {
        print.value = Math.round(evil(print.value) * 10000) / 10000;
        erase = true;
    } else if (x == 'sin'){
      print.value = 'Math.sin(' + print.value + ')';
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
    } else if (x == 'tan'){
      print.value = 'Math.tan(' + print.value + ')';
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
    } else if (x == 'log'){
      print.value = 'Math.log(' + print.value + ')';
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
    } else {
      print.value += x;
    }
};

  function evil(fn) {
    return new Function('return ' + fn)();
  }

$(document).ready(function() {
    if(!localStorage.getItem("selectedThemes")){
      localStorage.setItem("selectedThemes",'{"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"}');
    }
    background = JSON.parse(localStorage.getItem("selectedThemes"))["bcgColor"];
    font = JSON.parse(localStorage.getItem("selectedThemes"))["fontColor"];
    document.body.style.backgroundColor = background;
    document.body.style.color = font;
    $('.my-select').select2({
    'data': JSON.parse(localStorage.getItem("themes"))
    })
});

$('.apply-button').on('click', function(){  // sesuaikan class button
    var dipilih = $('.my-select option:selected').text()
    var panjang = JSON.parse(localStorage.getItem("themes")).length;

    var background = "";
    var font = "";

    for (var i = 0; i < panjang; i++) {
      if(JSON.parse(localStorage.getItem("themes"))[i]['text'] === dipilih){
        background = JSON.parse(localStorage.getItem("themes"))[i]["bcgColor"];
        font = JSON.parse(localStorage.getItem("themes"))[i]["fontColor"];
        localStorage.setItem("selectedThemes",JSON.stringify(JSON.parse(localStorage.getItem("themes"))[i]));
      }
    }

    document.body.style.backgroundColor = background;
    document.body.style.color = font;

    background = JSON.parse(localStorage.getItem("selectedThemes"))["bcgColor"];
    font = JSON.parse(localStorage.getItem("selectedThemes"))["fontColor"];
})
